import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ block, size }) => (
  <button
    className={`btn ${block ? 'btn-block' : ''} btn-${size} btn-primary`}
    type="button"
  >
  </button>
);

Button.propTypes = {
  block: PropTypes.bool,
  size: PropTypes.oneOf(['sm', 'md', 'lg']),
};

Button.defaultProps = {
  size: 'md',
};

export default Button;
